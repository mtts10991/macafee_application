<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'App\Http\Controllers\Mafee'], function () {
    Route::get('getlist', 'DashboaardController@get_list');
    Route::post('record', 'DashboaardController@set_data');
    Route::post('update_data', 'DashboaardController@update_data');
    Route::delete('delete_data/{id}', 'DashboaardController@destroy_data');
});
