<?php

namespace App\Http\Controllers\Mafee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\upload_data;

class DashboaardController extends Controller
{
    //
    public function get_list()
    {
        try {
            $data = upload_data::get();
            return response()->json($data);
        } catch (\Throwable $th) {
            return response()->json($th->getMessage());
        }
    }
    public function set_data(Request $request)
    {
        try {
            $data = upload_data::create([
                'cpu' => $request->cpu,
                'ram' => $request->ram,
                'mac_address' => $request->mac_address,
                'ip_address' => $request->ip_address,
                'user' => $request->user,
                'department' => $request->department,
                'remark' => $request->remark[0]
            ]);
            return response()->json($data);
        } catch (\Throwable $th) {
            return response()->json($th->getMessage());
        }
    }
    public function update_data($id, Request $request)
    {
        try {
            $data = upload_data::find($id);
            $data->cpu = $request->cpu? $request->cpu : $dara->cpu;
            $data->ram = $request->ram? $request->ram : $dara->ram;
            $data->mac_address = $request->mac_address? $request->mac_address : $dara->mac_address;
            $data->ip_address = $request->ip_address? $request->ip_address : $dara->ip_address;
            $data->department = $request->department? $request->department : $dara->department;
            $data->remark = $request->remark? json_decode($request->remark) : $dara->remark;
            return response()->json($data);
        } catch (\Throwable $th) {
            return response()->json($th->getMessage());
        }
    }
    public function destroy_data($id)
    {
        try {
            $data = upload_data::delete($id);
            return response()->json($data);
        } catch (\Throwable $th) {
            return response()->json($th->getMessage());
        }
    }
}
