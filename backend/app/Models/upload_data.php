<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class upload_data extends Model
{
    use HasFactory;
    protected $fillable = [
        'cpu',
        'ram',
        'mac_address',
        'ip_address',
        'user',
        'department',
        'remark',
    ];
    // protected $hidden = [
    //     'mac_address',
    //     'ip_address',
    // ];


}
