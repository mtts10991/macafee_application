<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_datas', function (Blueprint $table) {
            $table->id();
            $table->string('cpu');
            $table->string('ram');
            $table->string('mac_address');
            $table->string('ip_address');
            $table->string('user');
            $table->string('department');
            $table->string('remark');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upload_datas');
    }
};
